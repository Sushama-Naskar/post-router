import React, { Component } from 'react';
import Commentlist from './Commentlist';
import './postdetails.css';

import Usericon from '../images/user.png';
import Pagenotfound from './Pagenotfound';
import Cssloader from './Cssloader';


export default class Postdetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: null,
            user: null,
            comments: null,
            received: false,
            postId: null,
            iderror:false

        }

    }

    componentDidMount = () => {
        const id = this.props.match.params.id;
       if(id<=0 || id>100 || isNaN(id)){
        this.setState({
            iderror:true

        })
        return;
       }
        let post = null;
        let user = null;
        let postId = null;


        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`).then((res) => {
            return res.json();
        }).then((data) => {
            post = data;
            postId = data.id;

            return fetch(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
        }).then((res) => {
            return res.json()
        }).then((data) => {
            console.log(data);
            user = data;

            return fetch('https://jsonplaceholder.typicode.com/comments')

        }).then((res) => {
            return res.json();
        }).then((data) => {
            this.setState({
                post: post,
                user: user,
                comments: data,
                postId: postId,
                received: true

            })
        })


    }
    render() {
        // const id = this.props.match.params.id;
        // console.log(id);
        // console.log(this.state.post);
        const { post,
            user,
            comments,
            received,
            postId,
            iderror
        } = { ...this.state }
        console.log(comments, received)
        return (
            <div>
                {
                    (iderror)?<Pagenotfound/>:
                    (this.state.received) ?  <div key={postId} className="postdetails-main-container">
                    <div className='user-container'>
                    <div key={user.id}>
                        <div className='user-details'>
                        <div className='user-icon-div'><img src={Usericon} className='user-icon'/></div>
                        <div className='user-name'>
                        <div className="padding-bottom-6">@{user.username}</div>
                           <div>{user.name}</div>
                           </div>
                           </div>
                           
                        </div>
                   </div>
                   <div className="postdetails-container"> 
                    <div><h2>{post.title}</h2></div>
                    <div>{post.body}</div>
                    </div>
                    <div className="comment-container">
                    <Commentlist postId={postId} comments={comments}/>
                    </div>
                    </div> : <Cssloader/>
                }

            </div>

        )

    }




}


// <div className='user'>{this.state.user.name}</div>
// <Commentlist postId={this.state.postId} comments={this.state.comments} />