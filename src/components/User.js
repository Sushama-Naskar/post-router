import React, { Component } from 'react';
import Usericon1 from '../images/user.png';
import './user.css'
import {Link} from 'react-router-dom';
export default class Users extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render() {
        return (
            <div>
            {this.props.Users.map((Element)=>{
                if(Element.id===this.props.userId){
                    return(
                        <div key={Element.id}>
                        <div className='user-detail'>
                        <div className='user-icon'><img src={Usericon1} className='user-icon1'/></div>
                        <div className='user-name'>
                        <div className="padding-bottom-6"><Link to={{pathname:`/user/${this.props.userId}`}}>@{Element.username}</Link></div>
                           <div>{Element.name}</div>
                           </div>
                           </div>
                           
                        </div>
                    )
                }
                
            })}
                
            </div>
        )
    }
}
