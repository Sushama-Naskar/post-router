import React, { Component } from 'react';
import Chaticon from '../images/chat-bubble.png';
import './comment.css';
import User1 from '../images/user (1).png';
import Cssloader from './Cssloader';

export default class Comment extends Component {
    constructor(props){
        super(props);
        this.state={
            list:null,
            received:false,
            count:0
        }
    }
    countComments=(id)=>{
        let countComment=0;
        const commentPost=this.props.comments.filter((element)=>{
           
            return element.postId==id;
        })
        countComment=Object.keys(commentPost).length;
        
        return countComment;
    }
    show=(id)=>{
        let countComment=0;
        const commentPost=this.props.comments.filter((element)=>{
           
            return element.postId==id;
        })
      
       
        this.setState({
            list:commentPost,
            received: !this.state.received,
           
           
        })

    }
    render() {
       
        return (
            <div>
            <div className='icon-div'>
            <button onClick={()=>{this.show(this.props.postId)}}><div className='icon'><img src={Chaticon} className='chat-icon'/><div>{this.countComments(this.props.postId)} Comments</div> </div></button>
            </div>
           
                {
                    <div >
                    {(this.state.received)?<div>
                        {this.state.list.map((element)=>{
                            return(
                                <div key={element.id} className="comment">
                                <div className='comment-text'><div className='comment-user-icon'><img src={User1} className='user1-icon'/></div><div className='user-name'><div>{element.name} </div><div>{element.email}</div></div></div>
                                <div className='comment-body'>{element.body}</div>
                               

                                </div>
                            )
                        })}
                        </div>:<Cssloader/>}
                    </div>
                }
            </div>
        )
    }
}
