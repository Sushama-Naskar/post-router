import React, { Component } from 'react';
import Chaticon from '../images/chat-bubble.png';
import './commentlist.css';
import User1 from '../images/user (1).png';

export default class Commentlist extends Component {
    constructor(props) {
       super(props)
    }

    countComments=(id)=>{
        let countComment=0;
        const commentPost=this.props.comments.filter((element)=>{
           
            return element.postId==id;
        })
        countComment=Object.keys(commentPost).length;
        
        return countComment;

    }
    render() {
        return (
            <div >
            <div className='commentlist-icon-div'>
            <div><div className='commentlist-icon'>{this.countComments(this.props.postId)} Comments</div>
            </div>
            </div>
                {
           
                   this.props.comments.map((element) => {
                        if (element.postId ==this.props.postId) {
                            console.log(element.id)
                            return (
                                <div key={element.id} className="commentlist">
                                    <div className='commentlist-text'><div className='commentlist-user-icon'><img src={User1} className='commentlist-user1-icon' /></div><div className='commentlist-user-name'><div>{element.name} </div><div>{element.email}</div></div></div>
                                    <div className='commentlist-body'>{element.body}</div>


                                </div>
                            )

                        }


                    })
                }


            </div>
        )
    }
}