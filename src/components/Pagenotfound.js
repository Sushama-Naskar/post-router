import React, { Component } from 'react'
import './pagenotfound.css';

export default class Pagenotfound extends Component {
    render() {
        return (
            <div class="error404">
                <h1>404 Page Not Found</h1>
            </div>
        )
    }
}
