import React, { Component } from 'react';
// import Usericon from '../images/user.png';
import './userdetail.css';
import Usericon from '../images/user.png';
import Placeholdericon from '../images/placeholder.png';
import Suitcaseicon from '../images/suitcase.png';
import Phoneicon from '../images/phone.png';
import Emailicon from '../images/email.png';
import Worldicon from '../images/world-wide-web.png';
import Interneticon from '../images/internet.png';
import Pagenotfound from './Pagenotfound';
import Cssloader from './Cssloader';

export default class Users extends Component {
    constructor(props){
        super(props);
        this.state={
            user:null, 
            received:false,
            iderror:false
        }
       
    }
    componentDidMount(){
        const id=this.props.match.params.id;
        if(id<=0 || id>10 || isNaN(id)){
          this.setState({
              iderror:true
  
          })
          return;
         }
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`).then((res)=>{
          return res.json();
        }).then((data)=>{
            console.log(data)
          this.setState({
            user:data,
            received:true,
            iderror:false
            
          })

        })
  
    }
    render() {
      const {user,received,iderror}={...this.state};
        console.log(this.state.user)
       
       
        return (
          <div>

          {
            (iderror)?<Pagenotfound/>:
            (received) ?<div>
            
              <div key={user.id}>
                        <div className='user'>
                        <div class="userdetails-user-heading">
                            <div className='item-heading-1'><img src={Usericon} className="userdetails-user-icon" /> </div>
                            <div className='item-heading'><h2>{user.name}({user.username})</h2></div>
                            
                            <div className='item-heading-company'><img src={Placeholdericon} className='placeholder-icon' />{user.address.street}, {user.address.suite}, {user.address.city}-{user.address.zipcode}</div>
                            <div className='item-heading-company'><img src={Suitcaseicon} className='suitcase-icon' /><div>{user.company.name}, {user.company.catchPhrase}</div></div>
                            </div>
                            <div className="userdetails-contact">
                                <div className='item item-1'><div><img src={Phoneicon} className='phone-icon' /></div>{user.phone}</div>
                                <div className='item item-2'><div><img src={Emailicon} className='email-icon' /></div>{user.email}</div>
                                <div className='item item-3'><div><img src={Interneticon} className='internet-icon' /></div>{user.website}</div>
                            </div>

                           
                        </div>
                    </div>
            

            </div>:<Cssloader/>
          }
         
      
      </div>
        )
    
     
       
        
    }
}