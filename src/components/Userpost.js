import React, { Component } from 'react';
import Comment from './Comment';
import User from './User';
import './userpost.css';
import {Link} from 'react-router-dom';
import Cssloader from './Cssloader';

export default class Post extends Component {
    constructor(props){
        super(props);
        this.state={
          users:null,
          posts:null,
          comments:null,
          status:false
         
          
        }
      }
      componentDidMount(){
        fetch("https://jsonplaceholder.typicode.com/users").then((res)=>{
          return res.json();
        }).then((data)=>{
          
          this.setState({
            users:data,
            
          })
          return fetch("https://jsonplaceholder.typicode.com/posts");
        }).then((res)=>{
          return res.json();
        }).then((data)=>{
          
          this.setState({
            posts:data,
            
          })
          return fetch("https://jsonplaceholder.typicode.com/comments");
        }).then((res)=>{
          return res.json();
        }).then((data)=>{
          
          this.setState({
            comments:data,
            status:true
           
          })
          
        })
    
       
      }
      getUserName=(userid)=>{
            const {users,comments,posts,status}={...this.state};
            
            if(status){
              let name=users.find((user)=>{
              
                return user.id==userid;
              })
             
              return name.name;
            }
        }
    




    render() {
        
        return (
            <div>
            {
                (this.state.status)?
                this.state.posts.map((element)=>{
                    return(
                        <div key={element.id} className="main-container">
                        <div className='user-container'>
                         <User userId={element.userId} Users={this.state.users} />
                       </div>
                       <div className="post-container"> 
                        <div><h2><Link to={{pathname:`/post/${element.id}`}}>{element.title}</Link></h2></div>
                        <div>{element.body}</div>
                        </div>
                        <div className="comment-container">
                        <Comment postId={element.id} comments={this.state.comments}/>
                        </div>
                        </div>
                    
                    )
                }):<Cssloader/>
            }
           
           </div>
        )
    }
}

// <User userId={element.userId} Users={this.props.users} />
// <Comment postId={element.id} comments={this.state.comments}/>