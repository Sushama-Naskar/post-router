
import React,{Component} from 'react';

import {BrowserRouter as Router,Route, Switch } from 'react-router-dom';
import Postdetails from './components/Postdetails';
import Userdetails from './components/Userdetails';
import Pagenotfound from './components/Pagenotfound';
import Userpost from './components/Userpost'

class App extends Component {
  constructor(props){
    super(props);
 
  }


  render(){
  
   
   
    return (
     <Router>
      <div className="App">
     
    <div>
    <Switch>
    <Route exact path="/" component={Userpost}/>
      <Route  path="/post/:id" component={Postdetails}/>
      <Route  path="/user/:id" component={Userdetails}/>
      <Route path="*" component={Pagenotfound} />
     
 

      </Switch>
     
      </div>
          
      </div>
      </Router>
     
    );

    }
 
}

export default App;

// <Route exact path="/user"><Userdetails Users={this.state.users}/></Route>
// <Route  exact path="/" ><Userpost  posts={this.state.posts} comments={this.state.comments} getUserName={this.getUserName}/></Route>
